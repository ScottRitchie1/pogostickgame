﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ConfigurableJoint))]
public class PogoStickController : MonoBehaviour {
    public enum JumpType { none, passive, normal, boosted };

    Rigidbody rb;
    PogoStickCharacterContainer characterContainer;//character container that holds the character and controlls ik and detachments
    public PogoStickCharacterContainer CharacterContainer{get{return characterContainer;}}

    JumpType currentJumpStatus = JumpType.passive;
    public JumpType CurrentJump{get{return currentJumpStatus;}}

    bool isGrounded = false;
    public bool IsGrounded{get{return isGrounded;}}

    [Header("Jumping")]
    [SerializeField] float passiveJumpForce = 100;// amount we jump when no input is pressed - this is to keep momentum since the joint spring cant do it alone
    [SerializeField] float normalJumpForce = 250;// amount we jump when holding space
    [SerializeField] float boostedJumpForce = 500;//amount we jump when performing a boosted jump
    [Space]
    [SerializeField] float boostedJumpReleaseTime = 0.5f;// the time you need to release space before performing a jump to perform a boosted jump

    float jumpInputReleaseTimeStamp;// time since jump was released


    [Header("Rod Settings")]

    public float rodRadius = 0.075f;// size of the pogo rod
    [SerializeField] float pogoRodMaxLenght = 1;//max lenght the rod can be

    public float PogoRodLenght{
        get{return pogoRodMaxLenght;}
        set{UpdateRod(value);}
    }

    float pogoRodCurrentLenght = 0;
    public float PogoRodCurrentLenght{get{return pogoRodCurrentLenght;}}
    public MeshRenderer pogoRodVisuals;// the mesh for the rod
    CapsuleCollider pogoRodObject;//the actual rod object thatt eh joint is attached to
    public CapsuleCollider PogoRodObject { get { return pogoRodObject; } }

    [Header("Control Settings")]
    [SerializeField] float adjustmentTorque = 2;//how fast the pogostick aligns to its target grounded angle
    [SerializeField] float boostedJumpAdjustmentTorque = 4.5f;// the multipler to its adjust torque for when a boosted jump is performed
    const float adjustmentTorqueSmoothing = 50;//this is a const abratary value that makes sure the force applied to the rigid body is enough to keep up with the input direction movement


    [HideInInspector] public Vector3 targetLandAngle;//the angle that the pogostick should land at. calculated when it leaves the ground
    [HideInInspector] public float lastCompressionAmount;// the max compression the rod had last time it landed, calculated when jump force is applied
    [HideInInspector] public float landedAngleDot;// the amount the pogostick is alighed with its velocity. calculated when it hits the ground
    public RaycastHit lastGroundedHit;// the raycast hit where the pogostick is grounded, calculated while grounded

    LayerMask groundCheckLayer;// raycast layer to not hit ourselves

   
    [Header("Events")]
    public UnityEvent OnGroundedEvent = new UnityEvent();
    public UnityEvent OnUngroundedEvent = new UnityEvent();
    public UnityEvent OnJumpEvent = new UnityEvent();
    public UnityEvent OnBoostedJumpEvent = new UnityEvent();



    private void Awake()
    {
#if UNITY_EDITOR
        if (gameObject.layer != LayerMask.NameToLayer("Character"))
        {
            Debug.LogError("PogoStick needs to have the proper layer set. " + gameObject.name);
            gameObject.layer = LayerMask.NameToLayer("Character");
        }

        if(GetComponent<ConfigurableJoint>().connectedBody.gameObject.layer != LayerMask.NameToLayer("Character"))
        {
            Debug.LogError("PogoRod needs to have the proper layer set. " + gameObject.name);
            GetComponent<ConfigurableJoint>().connectedBody.gameObject.layer = LayerMask.NameToLayer("Character");
        }
#endif
        rb = GetComponent<Rigidbody>();
        pogoRodObject = GetComponent<ConfigurableJoint>().connectedBody.GetComponent<CapsuleCollider>();
        characterContainer = GetComponentInChildren<PogoStickCharacterContainer>();
        characterContainer.controller = this;

        groundCheckLayer = ~LayerMask.GetMask("Character", "Ignore Raycast");
    }

    // Use this for initialization
    void Start () {
        targetLandAngle = -Vector3.up;
        CamController.instance.target = characterContainer.characterAnim.transform.Find("Alpha:Hips");// set this to the camera target
    }

    private void Update()
    {
        //update in pogorod visuals
        if (pogoRodVisuals)
        {
            pogoRodVisuals.transform.localScale = new Vector3(pogoRodVisuals.transform.localScale.x, (pogoRodMaxLenght - pogoRodObject.transform.localPosition.y) / 2, pogoRodVisuals.transform.localScale.z);
            pogoRodVisuals.transform.localPosition = Vector3.up * (-(pogoRodMaxLenght - pogoRodObject.transform.localPosition.y) / 2);
        }
    }

    void FixedUpdate () {
        // jump input
        if (Input.GetKey(KeyCode.Space))
        {
            jumpInputReleaseTimeStamp = Time.time;
        }

        //calculate our torque to align with our targetlandangle
        float torqueMultiplier =  (currentJumpStatus == JumpType.boosted ? boostedJumpAdjustmentTorque : adjustmentTorque);

        targetLandAngle = Quaternion.AngleAxis(Input.GetAxis("Vertical") * torqueMultiplier * adjustmentTorqueSmoothing * Time.fixedDeltaTime, transform.right) * targetLandAngle;
        targetLandAngle = Quaternion.AngleAxis(-Input.GetAxis("Horizontal") * torqueMultiplier * adjustmentTorqueSmoothing * Time.fixedDeltaTime, transform.forward) * targetLandAngle;
        Vector3 uprightTorque = (-Vector3.Cross(transform.up, targetLandAngle)) * torqueMultiplier;

        //check if we are grounded
        if ((Vector3.Dot(rb.velocity.normalized, transform.up) < 0 || isGrounded) && Physics.SphereCast(transform.position + transform.up * (rodRadius * 2), rodRadius, - transform.up, out lastGroundedHit, pogoRodMaxLenght + rodRadius, groundCheckLayer))
        {
            if (isGrounded == false)
                OnGrounded();//initial on grounded call

            //update each fixed frame
            GroundedUpdate(uprightTorque);
        }
        else
        {
            if (isGrounded == true)
                OnUnGrounded();//initial leave ground call

            //update each fixed frame
            UnGroundedUpdate(uprightTorque);
        }
    }

    void GroundedUpdate(Vector3 uprightTorque)
    {
        //set the distance our rod is
        pogoRodCurrentLenght = Vector3.Distance(transform.position, lastGroundedHit.point);

        // if we are able to jump
        if (currentJumpStatus == JumpType.none && Vector3.Dot(rb.velocity, transform.up) > 0 && landedAngleDot > 0)
        {

            //calculate the amount the rod was compressed by before we jump
            lastCompressionAmount = Mathf.InverseLerp(pogoRodMaxLenght, 0, pogoRodCurrentLenght);
            float inputReleaseDetla = Time.time - jumpInputReleaseTimeStamp;
            
            //determine which kind of jump will be performed
            if (inputReleaseDetla > boostedJumpReleaseTime * lastCompressionAmount)
            {
                // if jump was released past our boosted jump threshhold its a passive jump
                //OnJump(JumpType.passive);
                OnJump(JumpType.normal);
            }
            else if(inputReleaseDetla > Time.fixedDeltaTime)
            {
                // if jump was released and is outside our current physics time step and smaller then our boosted stime threshhold this means we do a boosted jump
                OnJump(JumpType.boosted);
            }
            else 
            {
                // if jump hasnt been released and is within our current time step do a normal jump
                OnJump(JumpType.normal);
            }
        }

        //apply upright force scaled by our puright amount
        rb.AddTorque(uprightTorque * Vector3.Dot(transform.up, Vector3.up / 5), ForceMode.VelocityChange);

        // if we are stationary add downwards force to beable to gain jump momentum
        if(Mathf.Abs(rb.velocity.y) < 0.05f && lastCompressionAmount < 0.05f)
        {
            Vector3 downwardsForce = Vector3.down * normalJumpForce * 1.2f * (1 - lastCompressionAmount);
            rb.AddForce(downwardsForce, ForceMode.Acceleration);
        }

    }

    void UnGroundedUpdate(Vector3 uprightTorque)
    {
        // rotate pogo towards direction of the camera
        RotateToCamera();

        //stop overshooting and wobbling
        rb.AddTorque(-rb.angularVelocity * landedAngleDot, ForceMode.VelocityChange);
        //align to target land angle
        rb.AddTorque(uprightTorque * landedAngleDot, ForceMode.VelocityChange);
    }

    void OnGrounded()
    {
       
        targetLandAngle = -transform.up;
        isGrounded = true;
        //calculate the scaled amount we have landed in the direction we are traveling
        landedAngleDot = Mathf.Lerp(1, Mathf.Pow(Mathf.Clamp01(Vector3.Dot(-transform.up, rb.velocity.normalized)), 2), 1-lastCompressionAmount);
        currentJumpStatus = JumpType.none;
        OnGroundedEvent.Invoke();
    }

    void OnUnGrounded()
    {
        isGrounded = false;
        pogoRodCurrentLenght = pogoRodMaxLenght;

        // calculate the landing angle, should ne 3/4 the velocity and 1/4 up, with inverted y
        targetLandAngle = rb.velocity.normalized;
        targetLandAngle = new Vector3(targetLandAngle.x * 0.75f, -(targetLandAngle.y * 0.75f + 0.25f), targetLandAngle.z * 0.75f);
        //targetLandAngle = (targetLandAngle * 3 + Vector3.up) / 4;
        //targetLandAngle.y = -targetLandAngle.y;

        OnUngroundedEvent.Invoke();
    }

    void OnJump(JumpType jumpType)
    {
        // when we jump, called when we are at full rod compression and are traveling upwards
        OnJumpEvent.Invoke();

        if (jumpType == JumpType.normal)
        {
            currentJumpStatus = JumpType.normal;
            rb.AddForce(transform.up * normalJumpForce * landedAngleDot, ForceMode.Acceleration);
        }
        else if(jumpType == JumpType.boosted)
        {
            currentJumpStatus = JumpType.boosted;
            rb.AddForce(transform.up * boostedJumpForce * landedAngleDot, ForceMode.Acceleration);
            OnBoostedJumpEvent.Invoke();
        }
        else
        {
            currentJumpStatus = JumpType.passive;
            rb.AddForce(transform.up * passiveJumpForce * landedAngleDot * lastCompressionAmount, ForceMode.Acceleration);
        }
    }
    

    public void RotateToCamera()
    {
        //rotate the character to the orientation of the camera
        rb.AddTorque(Vector3.up * Vector3.SignedAngle(transform.forward, CamController.instance.GetCameraDirection(), Vector3.up) * Mathf.Clamp01(Vector3.Dot(transform.up, Vector3.up)), ForceMode.Acceleration);
    }

    void UpdateRod(float newHeight)
    {
        //updated the rod lenght
        pogoRodMaxLenght = newHeight;
        pogoRodObject.height = pogoRodMaxLenght;
        pogoRodObject.center = Vector3.up * (-pogoRodMaxLenght / 2);
    }
}

/*
//TRYING TO MAKE IT SO WHEN EXTERNAL ROTATION IS APPLIED WE DONT INSTANTLY TRY AND UOPRIGHT OUTSELVES wip
Vector3 clampedMAG = rb.angularVelocity;// Vector3.ClampMagnitude(rb.angularVelocity, Mathf.Sqrt(uprightTorque.magnitude / adjustmentTorque));
if (rb.angularVelocity.magnitude != clampedMAG.magnitude)
    Debug.Log("RB VEL MAG:" + rb.angularVelocity.magnitude + "    TORQUE VEL MA: " + uprightTorque.magnitude + "   CALCULATED VEL MAG: " + clampedMAG.magnitude);
*/


