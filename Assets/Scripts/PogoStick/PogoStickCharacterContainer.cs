﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(ConfigurableJoint))]
public class PogoStickCharacterContainer : MonoBehaviour {
    Rigidbody _rb;
    ConfigurableJoint _containerJoint;// joint that connects this container to the pogostick

    [HideInInspector] public PogoStickController controller;
    [HideInInspector] public Animator characterAnim;//animation component of the character
    


    [SerializeField] float leanSpeed = 5;// rate at which we lean to align with our lean angle


    // set positions and rotations for our joint
    [HideInInspector] Vector3 startAnchorPosition;
    [Header("Lean Positions")]
    [SerializeField] float forwardLeanLimit = 0.3f;
    [SerializeField] float backwardLeanLimit = 0.3f;
    [SerializeField] float sideLeanLimit = 0.25f;
    [SerializeField] Vector3 forwardAnchorPosition = new Vector3(-0.05f,0.15f,-0.1f);
    [SerializeField] Vector3 backwardsAnchorPosition = new Vector3(0, 0, -0.15f);
    [SerializeField] Vector3 sideAnchorPosition = new Vector3(-0.05f, -0.1f, 0);

    [Header("IK Positions")]
    public Transform rightFoot_ikPos;
    public Transform leftFoot_ikPos;
    public Transform rightHand_ikPos;
    public Transform leftHand_ikPos;

    //ik smoothing controllers
    float vel_RH;
    float vel_LH;
    float vel_RF;
    float vel_LF;

    float cur_RH_Weight = 0;
    float cur_LH_Weight = 0;
    float cur_RF_Weight = 0;
    float cur_LF_Weight = 0;


    [Header("Events")]
    public UnityEvent OnDetachEvent = new UnityEvent();

    private void Awake()
    {
#if UNITY_EDITOR
        if(gameObject.layer != LayerMask.NameToLayer("Character"))
        {
            Debug.LogError("CharacterContainer needs to have the proper layer set. " + gameObject.name);
            gameObject.layer = LayerMask.NameToLayer("Character");
        }
#endif
        _rb = GetComponent<Rigidbody>();
        _containerJoint = GetComponent<ConfigurableJoint>();
        characterAnim = GetComponentInChildren<Animator>();
    }

    void Start () {
        //disable collision between the pogo collider and character container
        Physics.IgnoreCollision(GetComponent<Collider>(), controller.GetComponent<Collider>(), true);
        Physics.IgnoreCollision(GetComponent<Collider>(), controller.PogoRodObject, true);

        startAnchorPosition = _containerJoint.connectedAnchor;

        characterAnim.SetBool("RH_IK", true);
        characterAnim.SetBool("LH_IK", true);
        characterAnim.SetBool("RF_IK", true);
        characterAnim.SetBool("LF_IK", true);


        InitializeRagdoll();
    }
    void InitializeRagdoll()
    {
        //initalize the ragdoll

        foreach(Rigidbody childRb in characterAnim.GetComponentsInChildren<Rigidbody>())
        {
            childRb.isKinematic = true;
        }

        foreach (Collider childCol in characterAnim.GetComponentsInChildren<Collider>())
        {
            childCol.enabled = false;
        }
    }

    void FixedUpdate () {
        UpdateContainerJoint();
    }

    private void Update()
    {
        //detach if we press 0
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            Detach();
        }
    }

    void UpdateContainerJoint()
    {
        //grab and scale target anchor
        Vector3 targetLandAngleRelative = -transform.InverseTransformDirection(controller.targetLandAngle);
        targetLandAngleRelative.y = 0.2f;
        targetLandAngleRelative.Normalize();

        //calculate joint anchor rotation
        Quaternion targetLean = new Quaternion(
            -targetLandAngleRelative.z * (targetLandAngleRelative.z > 0 ? forwardLeanLimit : backwardLeanLimit),0,
            targetLandAngleRelative.x * sideLeanLimit,1);

        //calculate joint anchor position
        Vector3 targetAnchor = startAnchorPosition + new Vector3(
            (targetLandAngleRelative.z > 0 ? forwardAnchorPosition.x : backwardsAnchorPosition.x) * targetLandAngleRelative.x + sideAnchorPosition.x * targetLandAngleRelative.x,
            (targetLandAngleRelative.z > 0 ? forwardAnchorPosition.y : backwardsAnchorPosition.y) * targetLandAngleRelative.z + sideAnchorPosition.y * Mathf.Abs(targetLandAngleRelative.x),
            (targetLandAngleRelative.z > 0 ? forwardAnchorPosition.z : backwardsAnchorPosition.z) * targetLandAngleRelative.z);

        //update anchor
        _containerJoint.targetRotation = Quaternion.Slerp(_containerJoint.targetRotation, targetLean, Time.deltaTime * leanSpeed);
        _containerJoint.connectedAnchor = Vector3.Lerp(_containerJoint.connectedAnchor, targetAnchor, Time.deltaTime * leanSpeed);
    }

    
    //Called to detach the player from the pogostick and deactive everything
    public void Detach()
    {
        Debug.Log("Detach");
        //FixedJoint j = controller.gameObject.AddComponent<FixedJoint>();
        //gameObject.transform.Find("Alpha@Alpha/Alpha:Hips/Alpha:Spine/Alpha:Spine1/Alpha:Spine2/Alpha:RightShoulder/Alpha:RightArm/Alpha:RightForeArm/Alpha:RightHand").gameObject.AddComponent<FixedJoint>();
        //j.connectedBody = gameObject.transform.Find("Alpha@Alpha/Alpha:Hips/Alpha:Spine/Alpha:Spine1/Alpha:Spine2/Alpha:RightShoulder/Alpha:RightArm/Alpha:RightForeArm/Alpha:RightHand").gameObject.GetComponent<Rigidbody>();
        //j.massScale = 100.0f;

        controller.GetComponent<Rigidbody>().isKinematic = false;

        //activate ragdoll parts
        foreach (Rigidbody childRb in characterAnim.GetComponentsInChildren<Rigidbody>())
        {
            childRb.isKinematic = false;
            childRb.velocity = _rb.velocity;
            childRb.angularVelocity = _rb.angularVelocity;

        }

        foreach (Collider childCol in characterAnim.GetComponentsInChildren<Collider>())
        {
            childCol.enabled = true;
        }
        GetComponent<Collider>().enabled = false;
        

        //Enable pogoColliders
        foreach (Collider childCol in controller.GetComponentsInChildren<Collider>())
        {
            if (childCol != controller.PogoRodObject)
                childCol.enabled = true;
        }

        //set pogo Rod Visuals to match pogo rod object
        if (controller.pogoRodVisuals)
        {
            controller.pogoRodVisuals.transform.localScale = new Vector3(controller.pogoRodVisuals.transform.localScale.x, controller.PogoRodLenght / 2, controller.pogoRodVisuals.transform.localScale.z);
            controller.pogoRodVisuals.transform.localPosition = Vector3.up * (-controller.PogoRodLenght / 2);
            controller.pogoRodVisuals.transform.SetParent(controller.PogoRodObject.transform);
        }


        // call event
        OnDetachEvent.Invoke();

        //destroy and remove att nessesary control components and objects
        characterAnim.transform.SetParent(null);
        Destroy(characterAnim);
        Destroy(controller);
        Destroy(gameObject);
        Destroy(controller.GetComponent<Collider>());
        Destroy(controller);
    }

    private void OnCollisionEnter(Collision collision)
    {
        // if we collide with something and arnt upright detach the character
        if( Vector3.Dot(controller.transform.up, Vector3.up) < 0.9)
        {
            Detach();
        }
    }

    //forwarded call from the animator. To Set ik positions
    public void OnAnimatorIK(int layerIndex)
    {
        cur_RH_Weight = Mathf.SmoothDamp(cur_RH_Weight, (characterAnim.GetBool("RH_IK") ? 1 : 0), ref vel_RH, 0.5f);
        characterAnim.SetIKPositionWeight(AvatarIKGoal.RightHand, cur_RH_Weight);
        characterAnim.SetIKRotationWeight(AvatarIKGoal.RightHand, cur_RH_Weight);
        characterAnim.SetIKPosition(AvatarIKGoal.RightHand, rightHand_ikPos.position);
        characterAnim.SetIKRotation(AvatarIKGoal.RightHand, rightHand_ikPos.rotation);

        cur_LH_Weight = Mathf.SmoothDamp(cur_LH_Weight, (characterAnim.GetBool("LH_IK") ? 1 : 0), ref vel_LH, 0.5f);
        characterAnim.SetIKPositionWeight(AvatarIKGoal.LeftHand, cur_LH_Weight);
        characterAnim.SetIKRotationWeight(AvatarIKGoal.LeftHand, cur_LH_Weight);
        characterAnim.SetIKPosition(AvatarIKGoal.LeftHand, leftHand_ikPos.position);
        characterAnim.SetIKRotation(AvatarIKGoal.LeftHand, leftHand_ikPos.rotation);

        cur_RF_Weight = Mathf.SmoothDamp(cur_RF_Weight, (characterAnim.GetBool("RF_IK") ? 1 : 0), ref vel_RF, 0.5f);
        characterAnim.SetIKPositionWeight(AvatarIKGoal.RightFoot, cur_RF_Weight);
        characterAnim.SetIKRotationWeight(AvatarIKGoal.RightFoot, cur_RF_Weight);
        characterAnim.SetIKPosition(AvatarIKGoal.RightFoot, rightFoot_ikPos.position);
        characterAnim.SetIKRotation(AvatarIKGoal.RightFoot, rightFoot_ikPos.rotation);

        cur_LF_Weight = Mathf.SmoothDamp(cur_LF_Weight, (characterAnim.GetBool("LF_IK") ? 1 : 0), ref vel_LF, 0.5f);
        characterAnim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, cur_LF_Weight);
        characterAnim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, cur_LF_Weight);
        characterAnim.SetIKPosition(AvatarIKGoal.LeftFoot, leftFoot_ikPos.position);
        characterAnim.SetIKRotation(AvatarIKGoal.LeftFoot, leftFoot_ikPos.rotation);
    }
}
