﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PogoEmotes : MonoBehaviour {

    Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

	void Update () {
        if (anim)
        {
            //play emotes if we click the right keys
            anim.SetBool("Wave", Input.GetKey(KeyCode.Alpha1));
            anim.SetBool("Dance", Input.GetKey(KeyCode.Alpha2));
            anim.SetBool("TPose", Input.GetKey(KeyCode.Alpha9));
        }
    }
}
