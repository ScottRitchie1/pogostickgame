﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PogoStickDebug : MonoBehaviour {
    public PogoStickController controller;

    List<Vector4> debugJumpPoints = new List<Vector4>();
    List<Vector4> debugLandPoints = new List<Vector4>();

    public Vector3 targetLandAngle;//the angle that the pogostick should land at. calculated when it leaves the ground
    public float lastCompressionAmount;// the max compression the rod had last time it landed, calculated when jump force is applied
    public float landedAngleDot;// the amount the pogostick is alighed with its velocity. calculated when it hits the ground
    public RaycastHit lastGroundedHit;// the raycast hit where the pogostick is grounded, calculated while grounded
    private void Awake()
    {
#if !UNITY_EDITOR
        Destroy(this);
#endif
        controller = GetComponent<PogoStickController>();
        controller.OnGroundedEvent.AddListener(OnPogoGrounded);
    }

    private void Update()
    {
        if (!controller)
        {
            Destroy(this);
        }
        
    }

    private void LateUpdate()
    {
        targetLandAngle = controller.targetLandAngle;
        lastCompressionAmount = controller.lastCompressionAmount;
        landedAngleDot = controller.landedAngleDot;
        lastGroundedHit = controller.lastGroundedHit;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        foreach (Vector4 vec in debugJumpPoints)
        {
            Gizmos.DrawRay(vec, Vector3.up * vec.w);

        }

        Gizmos.color = Color.red;

        foreach (Vector4 vec in debugLandPoints)
        {
            Debug.DrawRay(vec, Vector3.up * vec.w);
        }

        //rod gizmo
        if (controller.IsGrounded)
            Gizmos.color = Color.red;
        else
            Gizmos.color = Color.green;

        Gizmos.DrawLine(transform.position, transform.position + -transform.up * controller.PogoRodCurrentLenght);

        Gizmos.color = Color.black;
        Gizmos.DrawRay(transform.position, targetLandAngle * 2);
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position + transform.up * (controller.rodRadius * 2), controller.rodRadius);
        if (controller.IsGrounded)
        {
            Gizmos.color = Color.green;
        }
        Gizmos.DrawSphere(transform.position - transform.up * (controller.PogoRodLenght - controller.rodRadius), controller.rodRadius);
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 1000, 1000), controller.IsGrounded.ToString());
        GUI.Label(new Rect(10, 200, 1000, 1000), landedAngleDot.ToString());
        Texture2D textureImage = new Texture2D(2, 2, TextureFormat.ARGB32, false);
        Color imgColour = Color.red;


        if (controller.CurrentJump == PogoStickController.JumpType.boosted)
            imgColour = Color.blue;

        textureImage.SetPixel(0, 0, imgColour);
        textureImage.SetPixel(0, 1, imgColour);
        textureImage.SetPixel(1, 0, imgColour);
        textureImage.SetPixel(1, 1, imgColour);
        textureImage.Apply();

        GUI.DrawTexture(new Rect(10, 90, 100, 100), textureImage);
    }

    void OnPogoGrounded()
    {
        debugLandPoints.Add(new Vector4(controller.lastGroundedHit.point.x, lastGroundedHit.point.y, lastGroundedHit.point.z, landedAngleDot));

    }

    void OnPogoJump()
    {
        debugJumpPoints.Add(new Vector4(controller.lastGroundedHit.point.x, lastGroundedHit.point.y, lastGroundedHit.point.z, landedAngleDot));

    }
    [ContextMenu("Die")]// call from inspector to detach radgoll
    public void Die()
    {
        if (!Application.isPlaying)
            return;

        if (controller)
        {
            controller.CharacterContainer.Detach();
        }
    }

    [ContextMenu("Detach")]
    public void Detach()// call from the inspector to detach the character
    {
        if (!Application.isPlaying)
            return;
        //detach and destroy container
        foreach (Rigidbody childRb in controller.CharacterContainer.GetComponentsInChildren<Rigidbody>())
        {
            childRb.isKinematic = false;
            childRb.velocity = Vector3.up;
        }

        foreach (Collider childCol in controller.CharacterContainer.GetComponentsInChildren<Collider>())
        {
            childCol.enabled = true;
        }
        Destroy(controller.CharacterContainer.characterAnim);
    }
}
