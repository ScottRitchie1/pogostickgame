﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardOnAnimatorIK : MonoBehaviour {

    PogoStickCharacterContainer forwardTo;

    private void Start()
    {
        forwardTo = GetComponentInParent<PogoStickCharacterContainer>();
    }

    private void OnAnimatorIK(int layerIndex)
    {
        forwardTo.OnAnimatorIK(layerIndex);
    }
}
