﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jetpack : MonoBehaviour {

    Rigidbody rb;
    PogoStickController pogoAttachment;
    FixedJoint characterJoint;

    float remainingFuel = 0;
    public float fuelTime = 10;// seconds the jetpack is active for
    public bool active = false;
    public float power = 100;// thrust the jetpack has

    public Vector3 holdPosition;// position on the character container the jetpack is positioned

    public Text fuelText;
    public ParticleSystem jetParticles;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        remainingFuel = fuelTime;

        if (pogoAttachment)
        {
            Pickup(pogoAttachment);
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Input.GetKeyDown(KeyCode.LeftShift) && (pogoAttachment || characterJoint))// if we have a jetpack equipped and want to active it
        {
            active = true;
        }

        if (active)
        {
            // if active, remove fuel
            remainingFuel -= Time.fixedDeltaTime;

            if (remainingFuel > 0)
            {
                // if we have fuel add force depending on our state
                if (pogoAttachment)
                {
                    //attached to an active pogo
                    pogoAttachment.GetComponent<Rigidbody>().AddForce(pogoAttachment.transform.up * power);
                }
                else
                {
                    //attached to a character ragdoll
                    if (characterJoint)
                    {
                        characterJoint.connectedBody.AddForce(transform.up * power * 2);
                    }
                    else//attached to nothing
                    {
                        rb.AddForce(transform.up * power);

                    }
                }
            }
        }

        if (fuelText)//update our fuel indicator
        {
            float fuelPercent = Mathf.InverseLerp(0, fuelTime, remainingFuel);
            fuelText.text = Mathf.Round(fuelPercent * 100).ToString() + "%";
            if (active)
            {
                fuelText.color = Color.Lerp(Color.red, Color.green, fuelPercent);
            }
            else
            {
                fuelText.color = Color.white;
            }
        }
    }



    void Pickup(PogoStickController attachment)
    {
        if (attachment)
        {
            if (!attachment.CharacterContainer.GetComponentInChildren<Jetpack>() && !pogoAttachment && !characterJoint)
            {
                //attach the jetpack to a pogo stick
                pogoAttachment = attachment;
                pogoAttachment.CharacterContainer.OnDetachEvent.AddListener(OnPogoDetach);
                transform.SetParent(pogoAttachment.CharacterContainer.transform);
                transform.localPosition = holdPosition;
                transform.localRotation = Quaternion.identity;
                Physics.IgnoreCollision(GetComponent<Collider>(), pogoAttachment.CharacterContainer.GetComponent<Collider>(), true);
                rb.isKinematic = true;
            }
        }
        else if(attachment == null)
        {
            if (pogoAttachment)
            {
                //drop when on active pogostick
                transform.SetParent(null);
                rb.isKinematic = false;
                rb.velocity = pogoAttachment.GetComponent<Rigidbody>().velocity;
                rb.angularVelocity = pogoAttachment.GetComponent<Rigidbody>().angularVelocity;
                pogoAttachment.CharacterContainer.OnDetachEvent.RemoveListener(OnPogoDetach);
                Physics.IgnoreCollision(GetComponent<Collider>(), pogoAttachment.CharacterContainer.GetComponent<Collider>(), false);
                pogoAttachment = null;
            }else if (characterJoint)
            {
                //drop if connected to a ragdoll
                Destroy(characterJoint);
            }
        }
    }

    void OnPogoDetach()
    {
        if (pogoAttachment)
        {
            //detach the jetpack from the character container and attach it to the ragdoll using a joint
            transform.SetParent(null);
            rb.isKinematic = false;
            rb.velocity = pogoAttachment.GetComponent<Rigidbody>().velocity;
            pogoAttachment.CharacterContainer.OnDetachEvent.RemoveListener(OnPogoDetach);
            characterJoint = gameObject.AddComponent<FixedJoint>();
            characterJoint.connectedBody = pogoAttachment.CharacterContainer.transform.GetChild(0).GetChild(0).GetChild(2).GetChild(0).GetComponent<Rigidbody>();
        }
    }

    private void OnMouseDown()
    {
        // if we click on the jetpack pick up or drop it
        if (pogoAttachment)
        {
            Pickup(null);
        }
        else
        {
            Pickup(CamController.instance.target.gameObject.GetComponentInParent<PogoStickController>());
        }
    }
}
