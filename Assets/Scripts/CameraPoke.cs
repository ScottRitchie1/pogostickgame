﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraPoke : MonoBehaviour {

    // Update is called once per frame
    [SerializeField]
    float force = 100;

    public Transform PogoStickPrefab;
    public Transform spawnPoint;

    public int targetFPS = 60;

    [SerializeField] Vector3 pogoVelWhenUnlockedMouse;
    [SerializeField] Vector3 pogoAngVelWhenUnlockedMouse;
    private void Start()
    {
        if (targetFPS > 0)
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = targetFPS;
        }
        else
        {
            QualitySettings.vSyncCount = 1;
        }
    }
    void Update () {

        if (CamController.instance.target.root.GetComponent<PogoStickController>())
        {
            if (CamController.instance.lockCursor) // if we want to move
            {
                if (CamController.instance.target.root.GetComponent<Rigidbody>().isKinematic == true)
                {
                    CamController.instance.target.root.GetComponent<Rigidbody>().isKinematic = false;
                    CamController.instance.target.root.GetComponent<Rigidbody>().velocity = pogoVelWhenUnlockedMouse;
                    CamController.instance.target.root.GetComponent<Rigidbody>().angularVelocity = pogoAngVelWhenUnlockedMouse;
                }
            }
            else
            {
                if (CamController.instance.target.root.GetComponent<Rigidbody>().isKinematic == false)
                {
                    pogoVelWhenUnlockedMouse = CamController.instance.target.root.GetComponent<Rigidbody>().velocity;
                    pogoAngVelWhenUnlockedMouse = CamController.instance.target.root.GetComponent<Rigidbody>().angularVelocity;
                    CamController.instance.target.root.GetComponent<Rigidbody>().isKinematic = true;

                }
            }
        }
        // add force to rigidbody we click on
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject.GetComponent<Rigidbody>())
                    hit.collider.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(ray.direction * force, hit.point, ForceMode.Acceleration);
            }
        }

        //reset pogostick
        if (Input.GetKeyDown(KeyCode.Return) && spawnPoint && PogoStickPrefab)
        {
            if (GetComponent<CamController>().target.root.GetComponent<PogoStickController>())
            {
                GetComponent<CamController>().target.root.position += Vector3.up * 2;
                GetComponent<CamController>().target.root.rotation = Quaternion.identity;
                GetComponent<CamController>().target.root.GetComponent<PogoStickController>().targetLandAngle = -Vector3.up;
                GetComponent<CamController>().target.root.GetComponent<PogoStickController>().landedAngleDot = 1;
            }
            else
            {
                GetComponent<CamController>().target = Instantiate(PogoStickPrefab, CheckPoint.GetSpawnPointPosition(), Quaternion.identity);
            }

        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }
}
