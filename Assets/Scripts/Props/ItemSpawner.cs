﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSpawner : MonoBehaviour {

    public Transform spawnPoint;
    public GameObject itemToSpawn;

    public Text displayText;
    public Slider pogoLenghtSlider;

	public void SpawnItem()
    {
        Instantiate(itemToSpawn, spawnPoint.position, spawnPoint.rotation);
    }

    public void UpdateText()
    {
        displayText.text = pogoLenghtSlider.value.ToString();
    }

    public void SetRodLenght()
    {
        PogoStickController controller = CamController.instance.target.root.GetComponent<PogoStickController>();

        if (controller)
        {
            controller.PogoRodLenght = pogoLenghtSlider.value;
        }
    }
}
