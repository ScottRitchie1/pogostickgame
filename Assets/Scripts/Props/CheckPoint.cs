﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    public static CheckPoint currentCheckPoint;
    public Transform spawnPoint;
    public bool achived = false;
    public int priority = 0;

    public GameObject myMapObject;

    private void Start()
    {
        if (myMapObject)
        {
            myMapObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        if (myMapObject)
        {
            myMapObject.SetActive(false);
        }
    }

    public static Vector3 GetSpawnPointPosition()
    {
        if (currentCheckPoint)
        {
            return currentCheckPoint.spawnPoint.position;
        }

        return Vector3.one;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (achived == false && other.GetComponent<PogoStickController>())
        {
            if(currentCheckPoint == null || priority >= currentCheckPoint.priority){
                achived = true;
                currentCheckPoint = this;
                GetComponent<Animation>().Play();
                if (myMapObject)
                {
                    myMapObject.SetActive(true);
                }
            }
        }
    }
}
