﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxChanger : MonoBehaviour {
    [SerializeField] PogoStickController _controller;
    PogoStickController controller
    {
        set
        {
            if (_controller)// removed existing listeners
            {
                _controller.CharacterContainer.OnDetachEvent.RemoveListener(OnDetach);
                _controller.OnJumpEvent.RemoveListener(OnPogoJump);
            }
            _controller = value;

            if (_controller)//add listeners toi new object
            {
                _controller.CharacterContainer.OnDetachEvent.AddListener(OnDetach);
                _controller.OnJumpEvent.AddListener(OnPogoJump);

            }
        }
        get
        {
            return _controller;
        }
    }
	// Use this for initialization
	
	// Update is called once per frame
	void Update () {
        if (!controller)// look for new target
            controller = CamController.instance.target.transform.root.GetComponent<PogoStickController>();
    }

    void OnPogoJump()
    {
        // check if our target jumped on this box
        if(controller.lastGroundedHit.collider == gameObject.GetComponent<Collider>())
        {
            //create new material and randomize the colour
            Material newMat = new Material(GetComponent<MeshRenderer>().material);
            newMat.color = Random.ColorHSV(0, 1, 1, 1, 0, 1, 1, 1);
            GetComponent<MeshRenderer>().material = newMat;
        }
    }

    void OnDetach()
    {
        //called when the character is detached
        controller = null;
    }
}
